#!/bin/bash

url=`wget https://github.com/the-via/releases/releases/latest 2>&1 | grep Location`
rm latest

url=`echo $url | sed -e 's/Location: //g'`
url=`echo $url | sed -e 's/ \[following\]//g'`
url=`echo $url | sed -e 's/tag/download/g'`

version=`echo $url | sed -e 's:.*/v::'`

dl_url="$url/via-$version-linux.deb"

wget $dl_url --directory-prefix /tmp/ 

sudo dpkg -i /tmp/via-$version-linux.deb
