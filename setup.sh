#!/bin/bash

INSTALLS=false
DOTFILES=false

while getopts id option
do 
case "${option}"
in 
i) INSTALLS=true;;
d) DOTFILES=true;;
*) exit
esac
done

function install_packages {

# APT packages
echo "Installing APT packages..."
sudo apt-get install -y \
  vim \
  git \
  xclip \
  shellcheck \
  terminator \
  python3-pip \
  lolcat \
  remmina \
  neofetch \
  sass \
  nodejs \
  npm \
  firefox \
  libreoffice \
  steam \
  virt-manager \
  gimp \
  postgresql \
  libqt5webkit5 \
  ksh \
  xsel \
  maven \
  openjdk-8-jdk \
  ant \
  apache2 \
  libpcre3 \
  libpcre3-dev \
  vlc \
  binutils-dev \
  cmake \
  fonts-freefont-ttf \
  libsdl2-dev \
  libsdl2-ttf-dev \
  libspice-protocol-dev \
  libfontconfig1-dev \
  libx11-dev \
  nettle-dev \
  qemu-kvm \
  libvirt-clients \
  libvirt-daemon-system \
  bridge-utils \
  ovmf \


echo "Installing APT Packages... Done"

# Snap packages
echo "Installing Snap Packages..."
sudo snap install \
  spotify \
  dbeaver-ce \
  zoom-client \
  docker \

echo "Installing Snap Packages... Done"

# Snap packages (classic)
echo "Installing Snap packages (classic)..."
sudo snap install --classic code
sudo snap install --classic intellij-idea-ultimate 
sudo snap install --classic pycharm-professional 
sudo snap install --classic slack 

echo "Installing Snap packages (classic)... Done"

# Snap packages (edge)
echo "Installing Snap packages (edge)..."
sudo snap install --edge android-messages-desktop
echo "Installing Snap packages (edge)... Done"

# pip packages
echo "Installing pip3 packages..."
pip3 install \
  wget \
  psycopg2-binary \
  xlrd \
  requests \
  jinja2

echo "Installing pip3 packages... Done"

echo "Installing VIA Keyboard Software (latest)"
scripts/dl_via.sh || echo "VIA Install failed"
}

function move_dotfiles {
  cp -a dotfiles/. ~/
}

sudo apt-get update -y && sudo apt-get upgrade -y

if [ "$INSTALLS" == true ]; then
  echo "Calling install_packages"
  install_packages || echo "Installing packages failed"
fi

if [ "$DOTFILES" == true ]; then 
  echo "Calling move_dotfiles"
  move_dotfiles || echo "Moving dotfiles failed"
fi

# Setup alternative directory structures
mkdir -p ~/bin
mkdir -p ~/code

# Copy local bin scripts
cp -a bin/. ~/bin
